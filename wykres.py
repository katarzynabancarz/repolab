def read_text(nazwa):
    """Funkcja przyjmujaca nazwe pliku, czyta plik i zwraca slowa jako tablice"""
    tekst = []
    f = open(nazwa)

    for slowo in f.read().split():
        tekst.append(slowo)
    return tekst

#slowa = read_tekst('tekst.txt')

def histogram_show(plik):
    """Funkcja zwraca ile jest danych dlugosci slow"""
    lista = read_text(plik)
    slowa = {}

    for i in lista:
        licznik = 0
        dlugosc = len(i)
        for j in lista:
            if dlugosc == len(j):
                licznik += 1
        slowa[dlugosc] = licznik
        licznik = 0
    return slowa

def histogram_wykres(plik):
    """Rysuje wykres histogramu"""
    slownik = histogram_show(plik)
    for i in slownik:
        print i, '|', '='*slownik[i]


histogram_wykres('tekst.txt')

