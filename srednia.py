#(C) Kasia Bancarz
from myerror import MuhammadSasha
from tools import is_num, is_seq

def srednia(L):
    if not is_seq(L):
        raise MuhammadSasha
    if not is_num(L):
        raise MuhammadSasha
    
    s=0.0
    for i in range(len(L)):
        s+=L[i]
    w=s/len(L)
    return w

