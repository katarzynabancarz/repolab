def usun_znaki(L):
    """Usuwa znaki nie bedace literami z listy slow"""
    #print L

    niedozwolone=["'", '"', '.', ',', '<', '>', '/', '?', '!', '[', ']',
                  '{', '}', '(', ')', '*', '&', '^', '%', '$', '#', '@']

    ret=[]
    for slowo in L:
        for el in slowo:
            if el in niedozwolone:
                slowo=slowo.replace(el,"")
        ret.append(slowo.lower())
    
    return ret

